- [YouTube Data API v3  |  Google APIs Explorer](https://developers.google.com/apis-explorer/#p/youtube/v3/)
- [YouTube Data API Overview  |  Google Developers](https://developers.google.com/youtube/v3/getting-started)
- [YouTube Data API Reference  |  Google Developers](https://developers.google.com/youtube/v3/docs/)

- [How to change page results with YouTube Data API v3 - Stack Overflow](https://stackoverflow.com/questions/14173428/how-to-change-page-results-with-youtube-data-api-v3)
- [YouTube API to fetch all videos on a channel - Stack Overflow](https://stackoverflow.com/questions/18953499/youtube-api-to-fetch-all-videos-on-a-channel)

- [Учетные данные - YouTube - Google API Console](https://console.developers.google.com/apis/credentials)
